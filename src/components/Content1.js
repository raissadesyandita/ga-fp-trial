import React from 'react';
import { Jumbotron, Container, Row, Col, Button } from 'reactstrap';
import './Content1.css';
import Logo1 from '../images/Logo1.png';


const TopContent = () => {

    return (
        <>
            <Jumbotron fluid className="pb-0 mb-0">
                <Container fluid className="main-jumbotron">
                <Row>
                    <Col lg="6" className="contentjumb">
                        <h1 className="contenttext">
                        Stress Free<br />Subscription<br />Manager.</h1>
                        <Button className="button1">JOIN NOW</Button>
                    </Col>
                    <Col lg="6">
                        <img src={Logo1} alt="Logo" className="mb-0 pt-0 mt-0" />
                    </Col>
                </Row>
                </Container>
            </Jumbotron>
        </>
    )
}


export default TopContent;