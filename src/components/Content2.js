import React from 'react';
import { Container } from 'reactstrap';
import './Content2.css';
import App1 from '../images/apps/hbo-max.png';
import App2 from '../images/apps/disney+.png';
import App3 from '../images/apps/netflix.png';
import App4 from '../images/apps/spotify.png';
import Dots1 from '../images/CenterContent.png';


const Subscribe = () => {

    return (
        <div>
            <Container className="subscription ml-5 pt-4 mt-4 mb-4 pb-4">
            <h2 className="pt-4 mt-4">Subscribe to your favourite apps</h2>
            <img className="SubsApp" src={App1} alt="HBOMax" />
            <img className="SubsApp" src={App2} alt="Disney" />
            <img className="SubsApp" src={App3} alt="Netflix" />
            <img className="SubsApp" src={App4} alt="Spotify" />
            <img src={Dots1} alt="Dots" />
            <a href="/"><h3 className="linkapp">and many more...</h3></a>
            </Container>
        </div> 
    )
}

export default Subscribe;