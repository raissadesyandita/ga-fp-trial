import React from 'react';
import { Jumbotron, Button, Row, Col } from 'reactstrap';
import './Content3.css';
import Dots2 from '../images/BottomContent.png';
import Logo2 from '../images/Logo2.png';


const BottomContent = () => {

    return (
        <div>
            <Jumbotron fluid className="rectangle pt-0 pb-0 pb-0 mb-0">
                <Row>
                    <Col lg="7" className="land-left">
                        <h2>So what are you waiting for?</h2>
                        <Button className="button3">JOIN NOW</Button>
                        <img src={Dots2} alt="Dots" />
                    </Col>
                    <Col lg="5" className="land-right">
                        <img src={Logo2} alt="Logo" className="mt-auto" />
                    </Col>
                </Row>
            </Jumbotron>
        </div>
    )
}


export default BottomContent;