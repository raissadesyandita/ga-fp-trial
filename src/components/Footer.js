import React from 'react';
// import { Container } from 'reactstrap';
import './Footer.css';


const Footer = () => {
    return (
        <div className="container px-1">
            <div className="main-footer">
                <h4>2020 | <span style={{color:"#7971EA"}}>Fun</span>d.</h4>
            </div>
        </div>
    )
}


export default Footer;