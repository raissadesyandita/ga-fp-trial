import React, { useState } from 'react';
import {
  // Container,
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Button
  // NavbarText
} from 'reactstrap';
import './NavbarLanding.css';


const Header = (props) => {

  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);


    return (
      <div className="main-header">
        <div className="container mx-auto px-1">
        <Navbar expand="lg">
          <NavbarBrand href="/">
            <h3 style={{color:"black"}}><span style={{color:"#7971EA"}}>Fun</span>d.</h3>
          </NavbarBrand>
          <NavbarToggler onClick={toggle} />
          <Collapse isOpen={isOpen} navbar>
            <Nav className="header-link ml-auto" navbar>
              <NavItem className="header-link">
                <NavLink href="/">About</NavLink>
              </NavItem>
              <NavItem className="header-link">
                <NavLink href="/">Product</NavLink>
              </NavItem>
              <NavItem className="header-link">
                <NavLink href="/">Login</NavLink>
              </NavItem>
              <Button className="header-button">Sign Up</Button>{' '}
            </Nav>
          </Collapse>
        </Navbar>
        </div>
      </div>
    )
  }


export default Header;